
public class Saltarin extends Robot {
	
	public Saltarin (int identificador, int vida, int coordenada_x, int coordenada_y, int velocidad, int fuerza) 
                throws Robot_Exception
        {       
                super(identificador, vida, coordenada_x, coordenada_y, velocidad, fuerza);
                
                if (velocidad != 2) {
                    throw new Robot_Exception ("Saltarin: Velocidad erronea");
                }
                if (fuerza != 1 && fuerza != 3) {
                    throw new Robot_Exception ("Saltarin: fuerza erronea");
                }
	}
	
	public int[][] get_destino(int direccion, int posiciones){
            
		int [][] coordenadas_destino = new int [1][2];
				
		switch (direccion){
		
		case 1:
			
			coordenadas_destino[0][0]=coordenada_x;
			coordenadas_destino[0][1]=coordenada_y-velocidad;
		
		break;
		case 2: 				
                        coordenadas_destino[0][0]=coordenada_x-velocidad;
                        coordenadas_destino[0][1]=coordenada_y;
		break;
		case 3:
				
                        coordenadas_destino[0][0]=coordenada_x;
                        coordenadas_destino[0][1]=coordenada_y+velocidad;
		break;
		case 4:
				
                        coordenadas_destino[0][0]=coordenada_x+velocidad;
                        coordenadas_destino[0][1]=coordenada_y;
        break;
		}
			
		return coordenadas_destino;		
	}
}
