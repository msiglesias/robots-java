
public class Sesgado extends Robot {
	
	public Sesgado (int identificador, int vida, int coordenada_x, int coordenada_y, int velocidad, int fuerza) 
                throws Robot_Exception
        {
		super(identificador, vida, coordenada_x, coordenada_y, velocidad, fuerza);		
                if (fuerza != 5) {
                    throw new Robot_Exception ("Sesgado: fuerza erronea");
                }
	}
	

	public int[][] get_destino(int direccion, int posiciones){
            
		int [][] coordenadas_destino=new int[posiciones][2];                
		int multiplicador_x=0;
		int multiplicador_y=0;
		
		switch (direccion){
		case 1:
			multiplicador_x=-1;
			multiplicador_y=-1;			
		break;
		
		case 2:
                 multiplicador_x=-1;
                 multiplicador_y=1;
		break;
			
		case 3: 
			multiplicador_x=1;
			multiplicador_y=1;
					
		break;
		
		case 4: 
			multiplicador_x=1;
			multiplicador_y=-1;	
		break;
		
		}
		
		for(int i=0;i<posiciones;i++){
                    coordenadas_destino[i][0]=coordenada_x+(multiplicador_x*(i+1));
                    coordenadas_destino[i][1]=coordenada_y+(multiplicador_y*(i+1));  
		}
                
		return coordenadas_destino;
	}
}
