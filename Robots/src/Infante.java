
public class Infante extends Robot {
	
	public Infante (int identificador, int vida, int coordenada_x, int coordenada_y, int velocidad, int fuerza) 
                throws Robot_Exception
        {
		super(identificador, vida, coordenada_x, coordenada_y, velocidad, fuerza);
	}
	
	
	public int[][] get_destino(int direccion, int posiciones){
            
		int [][] coordenadas_destino=new int[this.velocidad][2];
		
                switch (direccion) {
                    case 1:
                        for(int i= 0; i < this.velocidad ; i++) {
                            coordenadas_destino[i][0]= coordenada_x;
                            coordenadas_destino[i][1]= coordenada_y-(i+1);
                        }
                        break;
                    case 2:
                        for(int i= 0; i < this.velocidad ; i++) {
                            coordenadas_destino[i][0]= coordenada_x-(i+1);
                            coordenadas_destino[i][1]= coordenada_y;
                        }
                        break;
                    case 3:
                        for(int i= 0; i < this.velocidad ; i++) {
                            coordenadas_destino[i][0]= coordenada_x;
                            coordenadas_destino[i][1]= coordenada_y+i+1;
                        }
                        break;
                    case 4:
                        for(int i= 0; i < this.velocidad ; i++) {
                            coordenadas_destino[i][0]= coordenada_x+i+1;
                            coordenadas_destino[i][1]= coordenada_y;
                        }
                        break;
                }
                
                
		return coordenadas_destino;
	}
	



}
