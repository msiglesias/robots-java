
public class Jugador {
	private String nombre;
	private String apellido;
	private Robot [] robots;
	
	
	public Jugador (String nombre, String apellido,Robot [] robots){
		this.nombre=nombre;
		this.apellido=apellido;
		this.robots=robots;
	}
		
	public String get_nombre(){
		return nombre;
	}
	
	public String get_apellido(){
		return apellido;
	}
	
	public void set_nombre(String nuevo_nombre){
		nombre=nuevo_nombre;
	}
	
	public void set_apellido(String nuevo_apellido){
		apellido =nuevo_apellido;
	}
	
	public Robot get_robot(int i){
		return robots[i];
	}
        
        public Robot [] conseguir_robots(){
        	return robots;
        }
        
        public void imprimir_caracteristicas(){
    		for (int i=0; i<robots.length; i++){
    			System.out.print("Robot: "+robots[i].get_identificador()+" ");
    			System.out.print("Vida: "+robots[i].get_vida()+" ");
    			System.out.print("Coordenada x: "+(robots[i].get_coordenada_x()+1)+" ");
    			System.out.print("Coordenada y: "+(robots[i].get_coordenada_y()+1)+" ");
    			System.out.print("Velocidad: "+robots[i].get_velocidad()+" ");
    			System.out.print("Fuerza: "+robots[i].get_fuerza());
    			System.out.println();
    		}
        }
}
