
abstract public class Robot {
	
	protected int identificador;
	protected int vida;
	protected int coordenada_x;
	protected int coordenada_y;
	protected int velocidad;
	protected int fuerza;
	
	
	public Robot(int identificador, int vida, int coordenada_x, int coordenada_y, int velocidad, int fuerza) 
                throws Robot_Exception
        {
                if (velocidad != 1 && velocidad != 2) {
                    throw new Robot_Exception ("Velocidad erronea");
                } else {
                    this.velocidad=velocidad;
                }
                if (fuerza != 1 && fuerza != 3 && fuerza != 5) {
                    throw new Robot_Exception ("Fuerza erronea");
                } else {
                    this.fuerza=fuerza;
                }
                if (vida > 100 || vida < 0) {
                    throw new Robot_Exception ("Vida erronea");
                } else {
                    this.vida = vida;
                }
                
		this.identificador=identificador;		
		this.coordenada_x=coordenada_x;
		this.coordenada_y=coordenada_y;		
		
	}
	
	public int get_identificador (){
		return identificador;
	}
	
	public int get_vida (){
		return vida;
	}

        public int get_coordenada_x (){
                return coordenada_x;
        }

        public int get_coordenada_y (){
                return coordenada_y;
        }

        public int get_velocidad (){
                return velocidad;
        }

        public int get_fuerza (){
                return fuerza;
        }


        public void set_identificador (int nuevo_identificador){
                identificador=nuevo_identificador;
        }

        public boolean set_vida (int nueva_vida){
            if(nueva_vida > 100 || nueva_vida < 0) {
                return false;
            } else {
                vida=nueva_vida;
                return true;
            }
        }

        public void set_coordenada_x (int nueva_coordenada_x){
                coordenada_x=nueva_coordenada_x;
        }

        public void set_coordenada_y (int nueva_coordenada_y){
                coordenada_y=nueva_coordenada_y;
        }

        public boolean set_velocidad (int nueva_velocidad){
                if (nueva_velocidad != 1 && nueva_velocidad != 2) {
                    return false;
                } else {
                    velocidad=nueva_velocidad;
                    return true;
                }                
        }

        public boolean set_fuerza (int nueva_fuerza){
                if (nueva_fuerza != 1 && nueva_fuerza != 3 && nueva_fuerza != 5) {
                    return false;
                } else {
                    fuerza=nueva_fuerza;
                    return true;
                }
        }
        
        public int [][] atacar(int direccion){
        	
        	int [][]casillas_atacadas=new int [this.fuerza][2];
        	int contador=0;
        	
        	switch (direccion){
        	case 1:
        		
        		switch (this.fuerza){
        		
        		case 1:
        			
    					for (int y=-1; y<=-1; y++) {
            			casillas_atacadas[contador][0]=this.coordenada_x;
            			casillas_atacadas[contador][1]=this.coordenada_y+y;
            			
            		
    					}
            		
        			break;
        			
        		case 3:
        			for (int y=-1; y<=-1; y++){
        				for (int x=-1; x<=1; x++) {
	            			casillas_atacadas[contador][0]=this.coordenada_x+x;
	            			casillas_atacadas[contador][1]=this.coordenada_y+y;
	            			contador++;
            		
    					}
            		}
        			break;
        			
        		case 5:
        			for (int y=-1; y<=0; y++){
    					
    					for (int x=-1; x<=1; x++) {
    						if (x==0 && y==00){
    							
    						}else{
    						casillas_atacadas[contador][0]=this.coordenada_x+x;
	            			casillas_atacadas[contador][1]=this.coordenada_y+y;
	            			contador++;
    						}
    					}
        			}
        			
        			break;
        		}        		
        		
        		break;
        		
        	case 2:
        		
        		switch (this.fuerza){
        		
        		case 1:
        			
    					for (int y=0; y<=0; y++) {
            			casillas_atacadas[contador][0]=this.coordenada_x-1;
            			casillas_atacadas[contador][1]=this.coordenada_y+y;
            			
            		
    					}
            		
        			break;
        			
        		case 3:
        			for (int x=-1; x<=-1; x++){
        				
        				for (int y=-1; y<=1; y++) {
	            			casillas_atacadas[contador][0]=this.coordenada_x+x;
	            			casillas_atacadas[contador][1]=this.coordenada_y+y;
	            			contador++;
            		
    					}
            		}
        			break;
        			
        		case 5:
        			for (int x=-1; x<=0; x++){
        				
        				for (int y=-1; y<=1; y++) {
        					if (x==0 && y==00){
    							
    						}else{
    						casillas_atacadas[contador][0]=this.coordenada_x+x;
	            			casillas_atacadas[contador][1]=this.coordenada_y+y;
	            			contador++;
    						}
    					}
        			}
        			
        			break;
        		} 
        		
        		
        		break;
        		
        	case 3:
        		
        		switch (this.fuerza){
        		
        		case 1:
        			
        			for (int y=1; y<=1; y++){
        				
        				for (int x=0; x<=0; x++) {
            			casillas_atacadas[contador][0]=this.coordenada_x+x;
            			casillas_atacadas[contador][1]=this.coordenada_y+y;
            			
            		
    					}
        			}
        			break;
        			
        		case 3:
        			for (int y=1; y<=1; y++){
        				
        				for (int x=-1; x<=1; x++) {
	            			casillas_atacadas[contador][0]=this.coordenada_x+x;
	            			casillas_atacadas[contador][1]=this.coordenada_y+y;
	            			contador++;
            		
    					}
            		}
        			break;
        			
        		case 5:
        			for (int y=0; y<=1; y++){
        				
        				for (int x=-1; x<=1; x++) {
        					if (x==0 && y==00){
    							
    						}else{
    						casillas_atacadas[contador][0]=this.coordenada_x+x;
	            			casillas_atacadas[contador][1]=this.coordenada_y+y;
	            			contador++;
    						}
    					}
        			}
        			
        			break;
        		} 
        		
        		
        		break;
        		
        	case 4:
        		
        		switch (this.fuerza){
        		
        		case 1:
        			
        			for (int x=1; x<=1; x++){
        				
        				for (int y=0; y<=0; y++) {
            			casillas_atacadas[contador][0]=this.coordenada_x+x;
            			casillas_atacadas[contador][1]=this.coordenada_y+y;
            			
            		
    					}
        			}
        			break;
        			
        		case 3:
        			for (int x=1; x<=1; x++){
        				
        				for (int y=-1; y<=1; y++) {
	            			casillas_atacadas[contador][0]=this.coordenada_x+x;
	            			casillas_atacadas[contador][1]=this.coordenada_y+y;
	            			contador++;
            		
    					}
            		}
        			break;
        			
        		case 5:
        			for (int x=0; x<=1; x++){
        				
        				for (int y=-1; y<=1; y++) {
        					if (x==0 && y==00){
    							
    						}else{
    						casillas_atacadas[contador][0]=this.coordenada_x+x;
	            			casillas_atacadas[contador][1]=this.coordenada_y+y;
	            			contador++;
    						}
    					}
        			}
        			
        			break;
        		}
        		
        		
        		break;
        	
        	
        	}
        	return casillas_atacadas;
        	
        }
        
        public void mover (int nueva_coordenada_x, int nueva_coordenada_y) {
            coordenada_x=nueva_coordenada_x;
            coordenada_y=nueva_coordenada_y;
        }
        
        abstract public int[][] get_destino(int direccion, int posiciones);
}
