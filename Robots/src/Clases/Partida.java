import java.util.*;

public class Partida {
    
	static Scanner leer=new Scanner(System.in);
	private Tablero tablero;
	private Jugador [] jugadores;
    private final int NUM_ROBOTS = 5;
	private int numero_jugadores;
	
	public Partida() throws Tablero_Exception, Robot_Exception{
		
		boolean correcto=false;
                
        System.out.println("Introduzca el numero de jugadores");
		numero_jugadores=leer.nextInt();                
        crear_jugador(NUM_ROBOTS);
		
			do{
				try{
					System.out.println("Introduzca las dimensiones del tablero: NxN, N debe ser mayor o igual a 8 y menor o igual a 20 ");
					int n=leer.nextInt();
					                               
                   tablero = new Tablero(n,conseguir_robots());                       
					correcto=true;
			
				}catch (Tablero_Exception te){
					System.out.println(te.getMessage());
			
				}catch (Robot_Exception re){
					System.out.println(re.getMessage());
				} catch (Casilla_Exception ex) {
                    System.out.println(ex.getMessage());
                                }    
			}while(correcto==false);
	}
	
		
	public void crear_jugador(int num_robots) throws Robot_Exception{
		
		String nombre;
		String apellido;
		jugadores=new Jugador [numero_jugadores];
                	
		int identificador=1;
		int vida=20;
		int coordenada_x=0;
		int coordenada_y=0;
		int velocidad;
		int fuerza;
                
		for (int i=0;i<numero_jugadores;i++){
			System.out.println("Jugador "+ (i+1));
			System.out.println("Introduzca nombre");
			nombre = leer.next();
			System.out.println("Introduzca apellido");
			apellido= leer.next();
			
                        Robot [] robots = new Robot [num_robots];
                        
                        for (int j = 0; j<num_robots; j++){				
			
                            if (j<2 || (j>4 && j<7) ){
                                    do{
                                        fuerza=(int)(Math.random()*(5))+1;
                                    }while(fuerza!=1 && fuerza!=3 && fuerza !=5);
                                    velocidad=(int)(Math.random()*(2))+1;
                                    robots [j]= new Infante(identificador, vida, coordenada_x, coordenada_y, velocidad, fuerza);
                            }

                            else if((j>1 && j<4) || (j>6 && j<9)){
                                    do{
                                            fuerza=(int)(Math.random()*(3))+1;
                                    }while(fuerza!=1 && fuerza!=3);
                                    velocidad=2;
                                    robots [j]= new Saltarin(identificador, vida, coordenada_x, coordenada_y, velocidad, fuerza);
                            }

                            else{
                                    fuerza=5;
                                    velocidad=(int)(Math.random()*(2))+1;
                                    robots [j]= new Sesgado(identificador, vida, coordenada_x, coordenada_y, velocidad, fuerza);
                            }
                            identificador++;
                        }
			
			jugadores [i]=new Jugador (nombre,apellido,robots);			 
		}	
	}
	
	public void jugar (){
		int turno_jugador=0;	
			
		comprobar_turno(turno_jugador);
		System.out.println("ganador: "+jugadores[0].get_nombre()+" "+jugadores[0].get_apellido());
	}
	
	
	public boolean perder(int turno_jugador){
		boolean perder= false;
		int contador=0;
		
		
			for (int j=0;j<NUM_ROBOTS;j++){
				if(jugadores[turno_jugador].get_robot(j).get_vida()>0){
					perder=false;
					break;
				}else{
					perder=true;
														
				}
			}
			
			if (perder){
				
				jugadores[turno_jugador]=null;
								
				Jugador [] jugadores_nuevos = new Jugador[numero_jugadores-1] ;
				
				//si un jugador ha perdido, no se le vuelve a tener en cuenta en la partida
				
				for (int p=0; p<numero_jugadores;p++){
					if(jugadores[p]!=null){
                                            jugadores_nuevos[contador]=jugadores[p];
                                            contador++;
					}
				}
                                
                                jugadores = jugadores_nuevos;
				
				if (turno_jugador==(numero_jugadores-1)){
					turno_jugador=0;
				
				}else{
                                    turno_jugador=(turno_jugador+1);
				}
				
				numero_jugadores=numero_jugadores-1;
			}
		
		
		return perder;
	}
	
	public void  comprobar_turno(int turno_jugador)  {
		String movimiento;
		int identificador;
		
		perder(turno_jugador);
		
		
		// Se comprueba el turno siempre que haya mas de 1 jugador en la partida, cuando quede 1 ese sera el ganador
		while(numero_jugadores>1){
			tablero.grafico_tablero();
			imprimir_caracteristicas();
                        
			perder(turno_jugador);
			
                        System.out.println("\nLe toca al jugador: "+jugadores[turno_jugador].get_nombre()+" "+jugadores[turno_jugador].get_apellido());
		
			do{
                                System.out.print("Seleccione robot, puede escoger entre: ");			
				for (int j=0;j<NUM_ROBOTS;j++){
					System.out.print(jugadores[turno_jugador].get_robot(j).get_identificador()+" ");
				}
				System.out.println();
                                
                                identificador=leer.nextInt();			
			
			}while(identificador<jugadores[turno_jugador].get_robot(0).get_identificador()||identificador>jugadores[turno_jugador].get_robot(NUM_ROBOTS-1).get_identificador());
			
			
			
			while(jugadores[turno_jugador].get_robot(identificador%NUM_ROBOTS).get_vida()<=0){
				System.out.println("Robot muerto");
				
				do{
					System.out.print("Seleccione robot, puede escoger entre: ");
					
					for (int j=0;j<NUM_ROBOTS;j++){
						System.out.print(jugadores[turno_jugador].get_robot(j).get_identificador()+" ");
					}
					identificador=leer.nextInt();
					
				}while(identificador<jugadores[turno_jugador].get_robot(0).get_identificador()||identificador>jugadores[turno_jugador].get_robot(NUM_ROBOTS-1).get_identificador());
			}
			
			identificador=((identificador-1)-(NUM_ROBOTS)*turno_jugador);
                        do{
                            System.out.println("mover o atacar");
                            movimiento=leer.next();
                        }while(!movimiento.equals("mover") && !movimiento.equals("atacar"));
		
                        if(movimiento.equals("mover")){

                                        mover_robot(identificador,turno_jugador);		

                                        if (turno_jugador==(numero_jugadores-1)){
                                                turno_jugador=0;
                                        }else{
                                            turno_jugador=(turno_jugador+1);
                                        }

                        }else{
                        				atacar_robot(identificador,turno_jugador);

                                        if (turno_jugador==(numero_jugadores-1)){
                                                turno_jugador=0;
                                        }else{
                                                turno_jugador=(turno_jugador+1);
                                        }							
                        }
                        evaluacion();		
		}		
	}
	
	public void atacar_robot(int identificador, int turno_jugador){
		int direccion;
		
		do{
			System.out.println("Donde desea atacar: 1.izquierda, 2.arriba, 3.derecha, 4.abajo");
			direccion=leer.nextInt();
			}while (direccion<1 || 4<direccion);
		
		tablero.realiza_ataque(jugadores[turno_jugador].get_robot(identificador%NUM_ROBOTS), direccion);
		
	}
        
        public void imprimir_caracteristicas() {
            for(int i = 0; i < numero_jugadores ;i++) {
                jugadores[i].imprimir_caracteristicas();
            }
        }
	
	public void mover_robot( int identificador, int turno_jugador){
            
		int direccion;
                int posiciones = 0;
		boolean movimiento_correcto=false;
					
		do{
                    System.out.println("Donde desea mover: 1.izquierda, 2.arriba, 3.derecha, 4.abajo, 5.diagonal");
                    direccion=leer.nextInt();

                    while((identificador==NUM_ROBOTS-1 && direccion !=5) || (identificador!= NUM_ROBOTS-1 && direccion==5)){
                            System.out.println("el robot elegido no puede moverse asi, vuelva a elegir movimiento");
                            System.out.println("Donde desea mover: 1.izquierda, 2.arriba, 3.derecha, 4.abajo, 5.diagonal");
                            direccion=leer.nextInt();
                    }
		
		}while(direccion<1 || direccion>5);
		
		if (direccion==5){
			do{
		 		   System.out.println("Hacia donde desea mover: 1.Noroeste, 2.Noreste, 3.Sureste, 4.Suroeste");
		 		   direccion=leer.nextInt();
		 		   }while(direccion<1 || direccion>4);

		 	do{
		 			System.out.println("cuantas casillas desea mover");
		 		 	posiciones=leer.nextInt();
		 	}while(posiciones<0 || posiciones>tablero.get_dimensiones());
		}
		
                movimiento_correcto=tablero.realiza_movimiento(jugadores[turno_jugador].get_robot(identificador%NUM_ROBOTS), direccion, posiciones);
		               
		if(movimiento_correcto==false){
			comprobar_turno(turno_jugador);
		}
		
        }
        
	public void evaluacion(){
		for (int i=0; i<numero_jugadores;i++){
                    for(int j= 0 ; j<NUM_ROBOTS; j++) {
			if(tablero.get_energia(jugadores[i].get_robot(j).get_coordenada_x(),jugadores[i].get_robot(j).get_coordenada_y())>0){
				jugadores[i].get_robot(j).set_vida(jugadores[i].get_robot(j).get_vida()+1);
				tablero.set_energia(tablero.get_energia(jugadores[i].get_robot(j).get_coordenada_x(),jugadores[i].get_robot(j).get_coordenada_y())-1,jugadores[i].get_robot(j).get_coordenada_x(),jugadores[i].get_robot(j).get_coordenada_y());
				
			}else if(tablero.get_energia(jugadores[i].get_robot(j).get_coordenada_x(),jugadores[i].get_robot(j).get_coordenada_y())<0){
				jugadores[i].get_robot(j).set_vida(jugadores[i].get_robot(j).get_vida()-1);
				tablero.set_energia(tablero.get_energia(jugadores[i].get_robot(j).get_coordenada_x(),jugadores[i].get_robot(j).get_coordenada_y())+1,jugadores[i].get_robot(j).get_coordenada_x(),jugadores[i].get_robot(j).get_coordenada_y());
			}
                    }
                }		
	}
        
        public Robot [] conseguir_robots() {
            Robot [] robots = new Robot [numero_jugadores*NUM_ROBOTS];
            Robot [] robot_jugador;
            
            for (int i = 0 ; i < numero_jugadores ; i++) {
                robot_jugador = this.jugadores[i].conseguir_robots();
                for (int j = 0; j < robot_jugador.length ; j++) {
                    robots[NUM_ROBOTS*i+j] = robot_jugador[j];
                }
            }
            
            return robots;
        }
}
