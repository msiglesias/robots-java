
enum TColor {Blanco,Negro}

public class Tablero {
	
	private Casilla[][] tablero;
	private int n;
	final private int MAX_N=20;
	final private int MIN_N=8;

	public Tablero(int n, Robot[] robots) throws Tablero_Exception, Robot_Exception, Casilla_Exception{
		
		if (n<MIN_N || n>MAX_N){
			throw new Tablero_Exception("Dimensiones erroneas");
		}else{
			this.n=n;
		}	
		
		tablero=new Casilla[n][n];
		
		TColor color=TColor.Blanco;
		int energia;
		
		for (int i=0; i<tablero.length; i++){
			for(int j=0; j<tablero[i].length; j++){
				energia=(int)(Math.random()*(5-(-5)))-5;
				tablero[i][j]=new Casilla(color,energia,null);
				if (color.equals(TColor.Blanco)){
					color=TColor.Negro;
				}
				else {
					color=TColor.Blanco;
				}
				
				
			}
			if (color.equals(TColor.Blanco)){
				color=TColor.Negro;
			}
			else {
				color=TColor.Blanco;
			}
		}
                
                coloca_robots(robots);                		
	}
	
	public boolean casilla_ocupada(int x, int y){
		return tablero[x][y].get_ocupado();
	}
        
        public TColor get_color (int x, int y){
		return tablero[x][y].get_color();
	}
        
        public int get_dimensiones () {
            return n;
        }
	
	public int get_energia (int x, int y){
		return tablero[x][y].get_energia();
	}

	public Robot get_robot(int x, int y){
		return tablero[x][y].get_robot();
	}
	
	public void set_color(TColor nuevo_color, int x, int y){
		tablero[x][y].set_color(nuevo_color);		
	}
	
	public boolean set_energia(int nueva_energia, int x, int y){
		return tablero[x][y].set_energia(nueva_energia);
	}
	
	public void set_ocupada(Robot nuevo_robot, int x, int y){
		tablero[x][y].set_ocupada(nuevo_robot);
	}
		
	public void coloca_robots(Robot[] robots) throws Robot_Exception {                
                for (int i=0;i<robots.length;i++){
			do{
				
				robots[i].set_coordenada_x((int)(Math.random()*((n)-0))+0);
				robots[i].set_coordenada_y((int)(Math.random()*((n)-0))+0);
			}while(tablero[robots[i].coordenada_x][robots[i].coordenada_y].get_ocupado());
				
			tablero[robots[i].coordenada_x][robots[i].coordenada_y].set_ocupada(robots[i]);
		}
	}
		
	public boolean realiza_movimiento (Robot robot, int direccion, int posiciones) {
            
            int [][] destino = robot.get_destino(direccion, posiciones);
            
            int ultimo = destino.length-1;
            
                       
            if(destino[ultimo][0]<0 || destino[ultimo][0]>this.n-1 || destino[ultimo][1] < 0 || destino[ultimo][1] > this.n-1) {
            	System.out.println("Se sale del tablero");
                return false;
            } else {
            	for(int i=0; i<destino.length;i++){
                	if(casilla_ocupada(destino[i][0],destino[i][1])){
                		System.out.println("Casilla ocupada");
                		return false;           		
                		
                	}
                }  
                
                this.set_ocupada(null, robot.get_coordenada_x(), robot.get_coordenada_y());                
                robot.mover(destino[ultimo][0], destino[ultimo][1]);
                this.set_ocupada(robot, destino[ultimo][0], destino[ultimo][1]);
                
                return true;
            }                        
        }
        
        public void realiza_ataque (Robot robot, int direccion) {
        	
        	int [][] casillas_atacadas = robot.atacar(direccion);
        	int vida_inicial;
        	int enemigos_atacados=0;
        	
        	
        	for(int i=0;i<casillas_atacadas.length;i++){
        		
        		
        		if (casillas_atacadas[i][0]<0 || casillas_atacadas[i][1]<0 || casillas_atacadas[i][0]>(this.n-1) || casillas_atacadas[i][1]>(this.n-1)){
        			
        		}
        		
        		else if(casilla_ocupada(casillas_atacadas[i][0],casillas_atacadas[i][1])){
        				vida_inicial=get_robot(casillas_atacadas[i][0],casillas_atacadas[i][1]).get_vida();
        				get_robot(casillas_atacadas[i][0],casillas_atacadas[i][1]).set_vida(vida_inicial-2);
        				enemigos_atacados++;
        			}
        		
        	}
        	
        	robot.set_vida(robot.get_vida()+(enemigos_atacados*5));
        	if(robot.get_vida()>=100){
        		robot.set_vida(100);
        	}
        	
        }
                
        public void grafico_tablero(){
    		
    		System.out.println("Energia\n");
    		
    		for (int i=0; i<tablero.length; i++){
    			for(int j=0; j<tablero[i].length; j++){
    				if (get_energia(i,j)>=0){
                                    System.out.print(" "+get_energia(i,j)+" ");
    				}else{
                                    System.out.print(get_energia(i,j)+" ");
    				}
    			}
    			System.out.println();
    		}
    		
    		System.out.println("\nPosicion\n");
    		for (int i=0; i<tablero.length; i++){
    			for(int j=0; j<tablero[i].length; j++){
    				if (get_robot(i,j) != null){
    					if(get_robot(i,j).get_vida()>0){
    							System.out.print(get_robot(i,j).get_identificador()+"  ");	
    					}else{
    						System.out.print("M"+ "  ");
    					}
    				}else{
    						System.out.print("X"+"  ");
    					}    				 
    			}
    			System.out.println();
    		}
    	}
    }

