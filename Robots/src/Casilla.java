
public class Casilla {
	
	private TColor color;
	private int energia;
	private Robot robot;
	
	
	
	public Casilla(TColor color, int energia, Robot robot) throws Casilla_Exception{
		this.color=color;
                
                if (energia < -5 || energia > 5) {
                    throw new Casilla_Exception ("Energia erronea");
                }else {
                    this.energia = energia;
                }
                
		this.robot=robot;	
	}
	
	public TColor get_color (){
		return color;
	}
	
	public int get_energia (){
		return energia;
	}

	public Robot get_robot(){
		return robot;
	}
	
	public void set_color(TColor nuevo_color){
		color=nuevo_color;		
	}
	
	public boolean set_energia(int nueva_energia){
                if (nueva_energia < -5 || nueva_energia > 5) {
                    return false;
                }else {
                    energia=nueva_energia;
                    return true;
                }
	}
	
	public void set_ocupada(Robot nuevo_robot){
		robot=nuevo_robot;
	}
	
	public boolean get_ocupado(){
		if (robot==null){
			return false;
		}else{
			return true;
		}
	}

}
